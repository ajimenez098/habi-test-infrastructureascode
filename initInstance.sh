#!/bin/bash

dirLogs=/home/ubuntu/logs/logs.log
DATE=`date +%Y%m%d_%H:%M:%S`

#Creación de carpeta de logs
cd /home/ubuntu/
mkdir logs
cd /home/ubuntu/logs/ 
echo "Comienza proceso de despliegue " >> /home/ubuntu/logs/logs.log
echo "Fecha despliegue: "$DATE" " >> $dirLogs

#CREACION DE VARIABLES DE ENTORNO
echo "Inicio creacion de variables de entorno " >> $dirLogs

sudo echo "export USER_GITLAB=\"{ENTER_USER_GITLAB}\"" >> /etc/environment
sudo echo "export PASS_GITLAB=\"{ENTER_PASS_GITLAB}\"" >> /etc/environment
sudo echo "export URL_GITLAB=\"{ENTER_URL_GITLAB}\"" >> /etc/environment
sudo echo "export TOKEN_GITLAB=\"{ENTER_TOKEN_GITLAB}\"" >> /etc/environment
sudo echo "export AWS_ACCESS_KEY_ID=\"{ENTER_AWS_ACCESS_KEY_ID}\"" >> /etc/environment
sudo echo "export AWS_SECRET_ACCESS_KEY=\"{ENTER_AWS_SECRET_ACCESS_KEY}\"" >> /etc/environment
source /etc/environment
echo "Finalizacion de creacion variables de entorno " >> $dirLogs

#ACTUALIZACION DE SISTEMA OPERATIVO/ INSTALACION DE PAQUETES
echo "Se comienza a actualizar la instancia y a instalar software necesario" >> $dirLogs
sudo apt-get update 
sudo apt -y install curl
sudo apt -y install git
sudo apt -y install python3
sudo apt-get -y install python3-pip
sudo apt-get -y install wget
sudo apt install unzip
cd /home/ubuntu/
mkdir instaladores
cd instaladores/
sudo curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
sudo unzip awscliv2.zip
sudo ./aws/install
echo "Actualizacion e instalacion de dependencias en servidor " >> $dirLogs

# CONFIGURACION GITLAB
wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod 777 /usr/local/bin/gitlab-runner
echo "Se descarga gitlabrunner  " >> $dirLogs

# CONFIGURACION DEL PROYECTO GIT
cd /home/ubuntu/
mkdir ProyectoHabi
cd ProyectoHabi/
git clone https://$USER_GITLAB:$PASS_GITLAB@$URL_GITLAB/root/test-habi.git /home/ubuntu/ProyectoHabi/
echo "Se descarga proyecto Git  " >> $dirLogs

pip install -r requirements.txt


