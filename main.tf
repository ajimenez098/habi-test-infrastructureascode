provider "aws"{
	region = var.aws_region
	access_key = var.aws_access_key
	secret_key = var.aws_secret_key
}

variable "ssh_key_path" {}
variable "aws_region" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "vpc_ip" {}
variable "subnet_ip" {}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] 
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-aws-testDevops"
  public_key = file(var.ssh_key_path)
}

resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_ip

  tags = {
    Name = "vpc-test-devops-habi"
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = var.subnet_ip
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = true

  tags = {
    Name = "subnet-test-devops-habi"
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Grupo de seguridad para instancia testDevOps"
  vpc_id      = aws_vpc.my_vpc.id 

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_security_group" "web-sg" {

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  tags = {
    Name = "web-sg"
  }
}

resource "aws_network_interface" "web" {
  subnet_id   = aws_subnet.my_subnet.id
  private_ips = ["172.50.10.100"]
  security_groups = [aws_security_group.allow_ssh.id]

  tags = {
    Name = "NI-test-devops-habi"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.my_vpc.id
  
  tags = {
    Name = "igw-testDevopsHabi"
  }
}

resource "aws_default_route_table" "public" {
  default_route_table_id = aws_vpc.my_vpc.main_route_table_id

  tags = {
    Name = "rt-testDevops-habi"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_default_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_default_route_table.public.id
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id 
  instance_type = "t3.medium"
  key_name = aws_key_pair.deployer.key_name
  user_data = "${file("initInstance.sh")}"
  
  network_interface {
    network_interface_id = aws_network_interface.web.id
    device_index         = 0
  }

  depends_on = [aws_internet_gateway.gw]
  
  tags = {
    Name = "InstanceTestDevOpsHabi"
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "habi-test"
  acl    = "private"
  versioning {
    enabled = true
  }
  
  tags = {
    Name = "habi-test"
  }
  
}

output "ip_instance" {
  value = aws_instance.web.public_ip
}

output "ssh" {
  value = "ssh -l ubuntu ${aws_instance.web.public_ip}"
}

